import pyglet
from math import sin, cos, radians
from time import time
from math import sin

class Grupo(list):
    def __init__(self, pos = (0,0), esc = (1,1), rot = 0):
        self.pos = pos
        self.esc = esc
        self.rot = rot
        super().__init__()
    
    def draw(self):
        pyglet.gl.glPushMatrix()
        pyglet.gl.glTranslated(self.pos[0], self.pos[1],0)
        pyglet.gl.glScaled(self.esc[0], self.esc[1], 0)
        pyglet.gl.glRotated(self.rot, 0,0,1)
        
        for elem in self:
            elem.draw()

        pyglet.gl.glPopMatrix()

class Fatia(Grupo):
    def __init__(self, ini, fim, raio, cor):
        tam = abs(fim - ini) + 2
        vert = [0.,0.]

        for i in range(ini, fim+1):
            ang = radians(i)
            vert += [sin(ang)*raio, cos(ang)*raio]

        self.vert_list = pyglet.graphics.vertex_list(tam, ('v2f', vert), 
                    ('c3B', cor * tam))
        super().__init__()

    def __del__(self):
        self.vert_list.delete()
    
    def draw(self):
        pyglet.gl.glPushMatrix()
        pyglet.gl.glTranslated(self.pos[0], self.pos[1],0)
        pyglet.gl.glScaled(self.esc[0], self.esc[1], 0)
        pyglet.gl.glRotated(self.rot, 0,0,1)

        self.vert_list.draw(pyglet.gl.GL_TRIANGLE_FAN)

        for elem in self:
            elem.draw()

        pyglet.gl.glPopMatrix()
        

window = pyglet.window.Window()

grupo1 = Grupo((window.width//2,window.height//2))
fatia1 = Fatia(0,30,200, (255,0,0))
fatia2 = Fatia(30,90,200, (255,0,255))
fatia2a = Fatia(30,90,100, (0,255,0))
fatia2.append(fatia2a)
fatia3 = Fatia(90,360,200, (255,255,0))
grupo1 += [fatia1,fatia2,fatia3]


@window.event
def on_draw():
    window.clear()
    fatia2.pos = (20,20)
    grupo1.draw()

@pyglet.clock.schedule
def update(dt):
    grupo1.rot += -1
    fatia2a.esc = (sin(time())+1,sin(time())+1) 


pyglet.app.run()
