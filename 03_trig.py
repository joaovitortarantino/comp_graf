import pyglet
from math import sqrt, cos, sin, pi
from time import time

window = pyglet.window.Window(width=1280, height=720)

sol = pyglet.shapes.Circle(640, 360, 100, color=(255,127,0))
terra = pyglet.shapes.Circle(940, 360, 30, color=(0,127,255))
lua = pyglet.shapes.Circle(980, 360, 10, color=(127,127,127))
linha = pyglet.shapes.Line(640,360,940,360, width=2, color=(255,255,255))
sol_terra = 300

def dist(p1, p2):
    return sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1])**2)

def polar(ang, dist):
    return (cos(ang)*dist, sin(ang)*dist)

def trans(p1, p2):
    return (p1[0]+p2[0], p1[1]+p2[1])

@window.event
def on_draw():
    window.clear()
    linha.draw()
    sol.draw()
    terra.draw()
    lua.draw()

@window.event
def on_mouse_press(x, y, button, modifiers):
    global sol_terra
    distancia = dist(sol.position, (x, y))
    if distancia > 110:
        sol_terra = distancia

@pyglet.clock.schedule
def update(dt):
    terra.position = trans(polar(time(), sol_terra), sol.position)
    lua.position = trans(polar(time()*3, 60), terra.position)
    linha.x2 = terra.x
    linha.y2 = terra.y



pyglet.app.run()